package com.test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author：lwj
 * @Date:2020/7/17 0017
 */
public class Test {
    public static void main(String[] args) {
        ArrayList<Person> arrayList = new ArrayList<>();
        for (int i = 0; i <5; i++) {
            Person person = new Person("Tom" + (i+1), i + 10, i % 2 == 0 ? 0 : 1);
            arrayList.add(person);
        }
        //重新建一个list
        List<Person> newlist=arrayList.stream().collect(Collectors.toList());
        //循环输出
        newlist.stream().forEach(p->{
            String s = p.getSex() == 0 ? "男" : "女";
            System.out.println(p.getName()+p.getAge()+s);
        });

        // 年龄倒序
        Comparator<Person> comparator = (p1, p2) -> p1.getAge().compareTo(p2.getAge());
        newlist.sort(comparator.reversed());
        System.out.println("=年龄倒序=  名字:{} -年龄：{} -性别:{}");
        newlist.stream().forEach(p -> {
            String s = p.getSex() == 0 ? "男" : "女";
            System.out.println(p.getName()+p.getAge()+s);
        });
    }
}
