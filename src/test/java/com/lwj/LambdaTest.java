package com.lwj;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @Author：lwj
 * @Date:2020/7/17 0017
 */
public class LambdaTest {
    public static void main(String[] args) {
        //新建框架
        JFrame frame=new JFrame();
        //新建一个画板
        JPanel panel=new JPanel();
        //创建一个按钮
        JButton button=new JButton("aa");

        //(jdk1.7版本)
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println("点击");
//            }
//        });
        /**
         * 通过监听器给按钮添加一个点击事件  (jdk1.8版本)
         *  lambda表达式写法  参数 -> 内容
         */
        button.addActionListener(event ->System.out.println("点击成功"));
        //设置是否可见
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(300,300);
        frame.add(panel);
        panel.add(button);


    }
}
